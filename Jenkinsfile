release = "v2019dev0"

/* Determine whether this is a production build and thus should be uploaded */
def production = env.JOB_NAME.startsWith("apertis-")

/* Registry to upload images to */
registry = "auth.docker-registry.apertis.org"
/* Credential id needed to upload to the registry */
registrycred = "apertis-docker"

def definitions = [
                    [ "apertis-image-builder",
                      "apertis/apertis-${release}-image-builder" ],
                    [ "apertis-documentation-builder",
                      "apertis/apertis-${release}-documentation-builder" ],
                    [ "apertis-testcases-builder",
                      "apertis/apertis-${release}-testcases-builder" ]
                  ]

def debosdefinitions = [
                         [ "apertis-package-builder",
                           "apertis/apertis-${release}-package-builder",
                           "apertis.yaml" ]
                       ]

def buildContainer(directory, name, upload = false) {
  docker.withRegistry("https://${registry}", registrycred) {
    d = docker.build("${registry}/${name}", "--no-cache ${directory}")
    if (upload) {
      d.push()
    }
  }
}

def buildContainerJob(directory, name, upload = false) {
  return {
    node("docker-slave") {
      stage("Checkout ${name}") {
        checkout scm
      }
      stage("Build ${name}") {
        buildContainer(directory, name, upload)
      }
    }
  }
}

def buildDebosContainerJob(directory, name, recipe, upload = false) {
  return {
    node("docker-slave") {
      stage("Checkout ${name}") {
        checkout scm
      }
      stage("Debos ${name}") {
        docker.withRegistry('https://docker-registry.apertis.org') {
          buildenv = docker.image("docker-registry.apertis.org/apertis/apertis-${release}-image-builder")
          /* Pull explicitely to ensure we have the latest */
          buildenv.pull()
          buildenv.inside("--device=/dev/kvm") {
            sh "cd ${directory} ; debos -t suite:${release} ${recipe}"
          }
        }
      }
      stage("Build ${name}") {
        buildContainer(directory, name, upload)
      }
    }
  }
}

def images = [:]

images += definitions.collectEntries { [ "Docker: ${it[0]}":
                                           buildContainerJob(it[0],
                                                             it[1],
                                                             production) ] }

images += debosdefinitions.collectEntries { [ "Docker: ${it[0]}":
                                                buildDebosContainerJob(it[0],
                                                                       it[1],
                                                                       it[2],
                                                                       production) ] }
parallel images
