#!/bin/sh
set -e

cp /bin/rm /bin/tar /usr/local/bin
apt-get update
DPKGPATH=/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
apt-get -y -o Dpkg::Path="$DPKGPATH" --allow-remove-essential purge mktemp
apt-get -y -o Dpkg::Path="$DPKGPATH" install coreutils tar
rm /usr/local/bin/rm /usr/local/bin/tar

